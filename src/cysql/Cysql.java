package cysql;

import java.sql.*;

import cysql.Info.CO_Author;
import cysql.Info.*;

public class Cysql {

    Connection connection;

    /**
     *
     */
    public Cysql() {
        try {
            DriverManager.registerDriver(new com.mysql.jdbc.Driver());
            String url = "jdbc:mysql://georgia.eng.utah.edu/acmdb08";
            connection = DriverManager.getConnection(url, "acmdbu08",
                    "2omn0428");
            System.out.println(connection.getCatalog());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /*
     * 执行select语句 例如"select * from UserInfo"
     */
    public ResultSet executeStatement(String s) {
        try {
            //Statement stmt = connection.createStatement();
            Statement stmt = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);

            ResultSet rs = null;
            if (stmt.execute(s)) {
                rs = stmt.getResultSet();
            }
            return rs;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /*
     * 例如传进来的s是。。。。
     * "create table UserInfo (userId int, userName varchar(20), userAddress varchar(20), userAge int check(userAge between 0 and 150), userSex varchar(20) default 'M' check(userSex='M' or userSex='W'))"
     * 创建成功返回true，否则是false；
     */
    public boolean createTable(String s) throws SQLException {
        try {
            Statement stat = connection.createStatement();
            stat.executeUpdate(s);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /*
     * 加一行。。。。
     */
    public boolean addBook(Book a) {
        try {
            String s = "insert into Book values(?,?,?,?,?,?,?,?,?,?)";
            PreparedStatement stmt = connection.prepareStatement(s);
            stmt.setString(1, a.ISBN);
            stmt.setString(2, a.title);
            stmt.setString(3, a.publisher);
            stmt.setInt(4, a.year);
            stmt.setInt(5, a.copies);
            stmt.setInt(6, a.sold);
            stmt.setDouble(7, a.Price);
            stmt.setString(8, a.format);
            stmt.setString(9, a.keywords);
            stmt.setString(10, a.subject);
            stmt.addBatch();
            stmt.executeUpdate();

            stmt.close();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean addCustomer(Customer a) {
        try {
            String s = "insert into " + Customer.tableName
                    + " values(?,?,?,?,?)";
            PreparedStatement stmt = connection.prepareStatement(s);
            stmt.setString(1, a.username);
            stmt.setString(2, a.fullName);
            stmt.setString(3, a.password);
            stmt.setString(4, a.address);
            stmt.setString(5, a.phoneNum);
            stmt.addBatch();
            stmt.executeUpdate();

            stmt.close();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean addAuthor(Author a) {
        try {
            System.out.println("add author   " + a.name);
            String s = "insert into " + Author.tableName + " values(?)";
            PreparedStatement stmt = connection.prepareStatement(s);
            stmt.setString(1, a.name);
            stmt.addBatch();
            stmt.executeUpdate();

            stmt.close();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean addCO_Author(CO_Author a) {
        try {
            String s = "insert into " + CO_Author.tableName + " values(?,?)";
            PreparedStatement stmt = connection.prepareStatement(s);
            stmt.setString(1, a.nameA);
            stmt.setString(2, a.nameB);
            stmt.addBatch();
            stmt.executeUpdate();

            stmt.close();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean addAuthorOf(AuthorOf a) {
        try {
            String s = "insert into " + AuthorOf.tableName + " values(?,?)";
            PreparedStatement stmt = connection.prepareStatement(s);
            stmt.setString(1, a.name);
            stmt.setString(2, a.ISBN);
            stmt.addBatch();
            stmt.executeUpdate();

            stmt.close();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean addOrder(Order a) {
        try {
            String s = "insert into " + Order.tableName
                    + " values(?,?,?,?,?,?)";
            PreparedStatement stmt = connection.prepareStatement(s);
            stmt.setInt(1, a.orderID);
            stmt.setString(2, a.username);
            stmt.setString(3, a.ISBN);
            stmt.setInt(4, a.copies);
            stmt.setInt(5, a.result);
            stmt.setString(6, a.information);
            stmt.addBatch();
            stmt.executeUpdate();

            stmt.close();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean addFeedBackRe(FeedBackRe a) {
        try {
            String s = "insert into " + FeedBackRe.tableName + " values(?,?,?,?)";
            PreparedStatement stmt = connection.prepareStatement(s);
            stmt.setString(1, a.ISBN);
            stmt.setString(2, a.username);
            stmt.setString(3, a.loginOfRe);
            stmt.setInt(4, a.isUseful);
            stmt.addBatch();
            stmt.executeUpdate();

            stmt.close();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean addFeedBack(FeedBack a) {
        try {
            String s = "insert into " + FeedBack.tableName + " values(?,?,?,?)";
            PreparedStatement stmt = connection.prepareStatement(s);
            stmt.setString(1, a.username);
            stmt.setString(2, a.ISBN);
            stmt.setString(3, a.text);
            stmt.setInt(4, a.rate);
            stmt.addBatch();
            stmt.executeUpdate();

            stmt.close();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }

    }

    public boolean addOpinion(Opinion a) {
        try {
            String s = "insert into " + Opinion.tableName + " values(?,?,?)";
            PreparedStatement stmt = connection.prepareStatement(s);
            stmt.setString(1, a.userA);
            stmt.setString(2, a.userB);
            stmt.setInt(3, a.isTrust);
            stmt.addBatch();
            stmt.executeUpdate();
            stmt.close();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static void main(String[] args) {
        // Cysql sql = new Cysql();

    }

}
