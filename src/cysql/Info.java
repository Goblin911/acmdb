package cysql;

public class Info {

    public static class PeoplePackage {
        public String[] useful;
        public String[] trustful;

        public String toString() {
            StringBuffer b = new StringBuffer();
            b.append("useful:\t");
            for (String s : useful)
                b.append(s + "\t");
            b.append("\n trustful:\t");
            for (String s : trustful)
                b.append(s + "\t");
            return b.toString();
        }
    }

    public static class BookPackage {
        public Book[] books;
        String[] authors;
        public String[] publishers;

        public String toString() {
            StringBuffer b = new StringBuffer();
            b.append("books:\n");
            for (Book s : books)
                b.append(s + "\n");
            b.append("\n authors:\n");
            for (String s : authors)
                b.append(s + "\t");
            b.append("\n publishers \n");
            for (String s : publishers)
                b.append(s + "\t");

            return b.toString();
        }

    }

    public static class Book {
        final static String tableName = "Book";
        public String ISBN;
        public String title;
        public String publisher;
        public Integer year;
        public Integer copies;
        public Integer sold; // 卖了几本。。。不要问。。。
        public Double Price;
        public String format;
        public String keywords;
        public String subject;

        @Override
        public String toString() {
            StringBuffer buffer = new StringBuffer();
            buffer.append("ISBN: " + ISBN);
            buffer.append("\ttitle: " + title);
            buffer.append("\tpublisher: " + publisher);
            buffer.append("\tyear: " + year);
            buffer.append("\tcopies: " + copies);
            buffer.append("\tsold: " + sold);
            buffer.append("\tPrice: " + Price);
            buffer.append("\tformat: " + format);
            buffer.append("\t keywords: " + keywords);
            buffer.append("\t subject: " + subject);
            return buffer.toString();
        }
    }

    public static class Customer {
        final static String tableName = "Customer";
        public String username;
        public String fullName;
        public String password;
        public String address;
        public String phoneNum;
    }

    public static class Author {
        final static String tableName = "Author";
        public String name;
    }

    public static class CO_Author {
        final static String tableName = "CO_Author";
        public String nameA, nameB;
    }

    public static class AuthorOf {
        final static String tableName = "AuthorOf";
        public String name, ISBN;
    }

    public static class Order {
        final static String tableName = "OrderInfo";
        public Integer orderID;
        public String username;
        public String ISBN;
        public Integer copies;
        public Integer result;
        public String information;
    }

    public static class FeedBack {
        final static String tableName = "FeedBack";
        public String username;
        public String ISBN;
        public String text;
        public Integer rate;

        public String toString() {
            String res = "";
            res += "username: " + username + "\tISBN: " + ISBN + "\ttext: "
                    + "\trate: " + rate;
            return res;

        }
    }

    public static class FeedBackRe {
        final static String tableName = "FeedBackRe";
        public String ISBN;
        public String username;
        public String loginOfRe;
        public Integer isUseful;
    }

    public static class Opinion {
        final static String tableName = "Opinion";
        public String userA, userB;
        public Integer isTrust;
    }

}
