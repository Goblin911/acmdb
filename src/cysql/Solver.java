package cysql;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.*;
import java.io.*;
import cysql.Info.*;

import cysql.Info.Book;

public class Solver {

    Cysql sql;
    boolean isValid;
    String username;
    int orderid = 0;

    /**
     *
     */
    public Solver() {
        sql = new Cysql();
        isValid = false;
        username = "";
    }

    private int getOrderID() {
        orderid++;
        return orderid;
    }

    public boolean haveBook(String ISBN) throws SQLException {
        String s = "Select * from Book B Where B.ISBN=\"" + ISBN + "\"";
        ResultSet rs = sql.executeStatement(s);
        if (rs.first())
            return true;
        return false;
    }

    public boolean haveUser(String username) throws SQLException {
        String s = "Select * from Customer C Where C.username=\"" + username
                + "\"";
        ResultSet rs = sql.executeStatement(s);
        if (rs.first())
            return true;
        return false;
    }

    // 返回是否成功 0 no such user 1: succ -1:wrong password
    public Integer login(String username, String password) {
        String s = "Select username, password from Customer C Where C.username=\""
                + username + "\"";
        ResultSet rs = sql.executeStatement(s);
        if (rs == null)
            return -1;
        try {
            if (!rs.first()) {
                rs.close();
                this.username = username;
                return 0;
            }
            for (rs.first(); !rs.isAfterLast(); rs.next())
                if (rs.getString(2).equals(password)) {
                    this.isValid = true;
                    this.username = username;
                    rs.close();
                    return 1;
                }
            return -1;
        } catch (SQLException e) {
            return null;
        }
    }

    // 新建一个用户，返回是否成功
    public boolean create(Info.Customer t) {
        isValid = sql.addCustomer(t);
        return isValid;
    }

    // 如果不合法，返回null，否则返回int,没书返回-1
    public Integer order(String ISBN, Integer num) {
        if (!isValid)
            return null;
        String s = "Select * from Book B Where B.ISBN=\"" + ISBN
                + "\"";
        System.out.println(s);
        ResultSet rs = sql.executeStatement(s);
        System.out.println(rs);
        if (rs == null)
            return null;
        try {
            if (!rs.first()) {
                System.out.println("fuck");
                return null;
            }
            rs.first();
            int ret = Math.min(num, rs.getInt("copies"));
            rs.updateInt("copies", rs.getInt("copies") - ret);
            rs.updateInt("sold", rs.getInt("sold") + ret);
            rs.updateRow();
            rs.close();
            Info.Order order = new Info.Order();
            order.copies = num;
            order.information = "";
            order.ISBN = ISBN;
            order.username = this.username;
            order.result = ret;
            order.orderID = this.getOrderID();
            sql.addOrder(order);
            return ret;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    // 加入新书，可以加入返回true
    public boolean addBook(Info.Book book, LinkedList<Info.Author> authors) {
        try {
            if (this.haveBook(book.ISBN))
                return false;
            if (!sql.addBook(book)) {
                return false;
            }
            for (int i = 0; i < authors.size(); i++) {
                String s = "Select * from Author A Where A.name=\""
                        + authors.get(i).name + "\"";
                ResultSet rs = sql.executeStatement(s);
                if (!rs.first())
                    sql.addAuthor(authors.get(i));
                rs.close();
                Info.AuthorOf ao = new Info.AuthorOf();
                ao.ISBN = book.ISBN;
                ao.name = authors.get(i).name;
                sql.addAuthorOf(ao);
            }
            for (int i = 0; i < authors.size(); i++)
                for (int j = i + 1; j < authors.size(); j++) {
                    String s = "Select * from CO_Author CA Where (CA.nameA=\""
                            + authors.get(i).name + "\" AND CA.nameB=\""
                            + authors.get(j).name + "\"" + ")";
                    //System.out.println(s);
                    ResultSet rs = sql.executeStatement(s);
                    if (!rs.first()) {
                        Info.CO_Author ca = new Info.CO_Author();
                        ca.nameA = authors.get(i).name;
                        ca.nameB = authors.get(j).name;
                        sql.addCO_Author(ca);
                        ca.nameA = authors.get(j).name;
                        ca.nameB = authors.get(i).name;
                        sql.addCO_Author(ca);
                    }
                    rs.close();
                }
            return true;
        } catch (SQLException e) {
            return false;
        }
    }

    // 修改书的copy数，成功返回true
    public boolean changeCopy(String ISBN, Integer copy) {
        String s = "Select * from Book B Where B.ISBN=\"" + ISBN
                + "\"";
        System.out.println(s);
        ResultSet rs = sql.executeStatement(s);
        try {
            if (!rs.first())
                return false;
            rs.updateInt("copies", rs.getInt("copies") + copy);
            rs.updateRow();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    // 评论
    public boolean feedback(String ISBN, String text, Integer rate) {
        if (!isValid)
            return false;
        try {
            if (!this.haveBook(ISBN))
                return false;
            Info.FeedBack fb = new Info.FeedBack();
            fb.ISBN = ISBN;
            fb.rate = rate;
            fb.text = text;
            fb.username = this.username;
            if (!sql.addFeedBack(fb))
                return false;
            return true;
        } catch (SQLException e) {
            return false;
        }

    }

    public boolean feedbackUseful(String ISBN, String username, Integer isUseful) {
         if (!isValid)
            return false;
       try {
            if ((!this.haveBook(ISBN)) || (!this.haveUser(username)))
                return false;
            Info.FeedBackRe fbr = new Info.FeedBackRe();
            fbr.username = this.username;
            fbr.loginOfRe = username;
            fbr.isUseful = isUseful;
            fbr.ISBN = ISBN;
            return sql.addFeedBackRe(fbr);
        } catch (SQLException e) {
            return false;
        }
    }

    public boolean userTrustfull(String username, boolean isTrustful) {
          if (!isValid)
            return false;
      try {
            if (!this.haveUser(username))
                return false;
            Info.Opinion op = new Info.Opinion();
            if (isTrustful)
                op.isTrust = 1;
            else
                op.isTrust = -1;
            op.userA = this.username;
            op.userB = username;
            return sql.addOpinion(op);
        } catch (SQLException e) {
            return false;
        }
    }

    /*
     * ch = 'a' or 'b' or 'c'
     */
    public Info.Book[] search(String author, String publisher, String title,
            String subject, char ch) {
           if (!isValid)
            return null;
     if (author == null && publisher == null && title == null
                && subject == null)
            return null;
        String checkStatment = "";
        boolean bool = false;
        if (publisher != null) {
            if (bool)
                checkStatment += " AND ";
            else
                bool = true;
            checkStatment += "B.publisher =\"" + publisher + "\"";
        }
        if (title != null) {
            if (bool)
                checkStatment += " AND ";
            else
                bool = true;
            checkStatment += "B.title Like \"%" + title + "%\"";
        }
        if (subject != null) {
            if (bool)
                checkStatment += " AND ";
            else
                bool = true;
            checkStatment += "B.subject=\"" + subject + "\"";
        }
        if (author != null) {
            if (bool)
                checkStatment += " AND ";
            else
                bool = true;
            checkStatment += "EXISTS (Select * from AuthorOf ao WHERE ao.name=\""
                    + author + "\" AND B.ISBN=ao.ISBN)";
        }
        String s = "";
        if (ch == 'a')
            s = "Select Distinct * from Book B Where " + checkStatment
                    + " ORDER BY year";
        if (ch == 'b')
            s = "Select Distinct B.*,AVG(F.rate)  AS score from Book B,FeedBack F Where "
                    + checkStatment
                    + " AND B.ISBN=F.ISBN GROUP BY B.ISBN ORDER BY score DESC";
        if (ch == 'c')
            s = "Select Distinct B.*,AVG(F.rate) AS score_t from Book B,FeedBack F "
                    + "Where " + checkStatment + " AND B.ISBN=F.ISBN AND "
                    + "EXISTS (Select * from Opinion O "
                    + "WHERE O.userB=F.username AND isTrust=1)"
                    + "GROUP BY B.ISBN" + " ORDER BY score_t DESC";
        System.out.println(s);
        ResultSet rs = sql.executeStatement(s);
        LinkedList<Book> books = new LinkedList<>();
        try {
            if (!rs.first())
                return new Book[0];
            while (!rs.isAfterLast()) {
                Book book = new Book();
                book.ISBN = rs.getString("ISBN");
                book.copies = rs.getInt("copies");
                book.format = rs.getString("format");
                book.keywords = rs.getString("keywords");
                book.Price = rs.getDouble("Price");
                book.publisher = rs.getString("publisher");
                book.sold = rs.getInt("sold");
                book.subject = rs.getString("subject");
                book.title = rs.getString("title");
                book.year = rs.getInt("year");
                books.add(book);
                rs.next();
            }
            Book[] ret = new Book[books.size()];
            for (int i = 0; i < books.size(); i++)
                ret[i] = books.get(i);
            return ret;
        } catch (SQLException e) {
            return null;
        }
    }

    public Info.FeedBack[] search(String ISBN, int n) {
            if (!isValid)
            return null;
    String s = "Select Distinct F.*, AVG(FR.isUseful) AS score from FeedBack F, FeedBackRe FR "
                + "WHERE F.username=FR.loginOfRe AND F.ISBN=FR.ISBN AND F.ISBN=\""
                + ISBN + "\"" + " GROUP BY F.username " + "ORDER BY score DESC";
    System.out.println(s);
        ResultSet rs = sql.executeStatement(s);
        try {
            if (!rs.first())
                return new Info.FeedBack[0];
            int ii = 0;
            LinkedList<Info.FeedBack> feedbacks = new LinkedList<>();
            while (ii < n && !rs.isAfterLast()) {
                ii++;
                Info.FeedBack fb = new Info.FeedBack();
                fb.ISBN = rs.getString("ISBN");
                fb.rate = rs.getInt("rate");
                fb.text = rs.getString("text");
                fb.username = rs.getString("username");
                feedbacks.add(fb);
                rs.next();
            }
            Info.FeedBack[] ret = new Info.FeedBack[feedbacks.size()];
            for (int i = 0; i < feedbacks.size(); i++)
                ret[i] = feedbacks.get(i);
            return ret;
        } catch (SQLException e) {
            return null;
        }
    }

    public Info.Book[] recommendation(String ISBN) {
             if (!isValid)
            return null;
   String s = "Select * from Book B " + "WHERE B.ISBN<>\"" + ISBN
                + "\" AND EXISTS " + "(Select * from OrderInfo O, OrderInfo O_o"
                + " WHERE O.username=O_o.username AND O.ISBN=\"" + ISBN + "\" AND O_o.ISBN=B.ISBN)"
                + " ORDER BY sold DESC";
        System.out.println(s);
        ResultSet rs = sql.executeStatement(s);
        LinkedList<Book> books = new LinkedList<>();
        try {
            if (!rs.first())
                return new Book[0];
            while (!rs.isAfterLast()) {
                Book book = new Book();
                book.ISBN = rs.getString("ISBN");
                book.copies = rs.getInt("copies");
                book.format = rs.getString("format");
                book.keywords = rs.getString("keywords");
                book.Price = rs.getDouble("Price");
                book.publisher = rs.getString("publisher");
                book.sold = rs.getInt("sold");
                book.subject = rs.getString("subject");
                book.title = rs.getString("title");
                book.year = rs.getInt("year");
                books.add(book);
                rs.next();
            }
            Book[] ret = new Book[books.size()];
            for (int i = 0; i < books.size(); i++)
                ret[i] = books.get(i);
            return ret;
        } catch (SQLException e) {
            return null;
        }
    }

    public Integer bfs(String authorA, String authorB) {
        LinkedList<String> list = new LinkedList<>();
        HashMap<String, Integer> ans = new HashMap<>();
        String s = "Select * from Author A " + "Where A.name=\"" + authorA
                + "\"";
        ResultSet rs = sql.executeStatement(s);
        try {
            if (!rs.first())
                return null;
            s = "Select * from Author A " + "Where A.name=\"" + authorB + "\"";
            rs = sql.executeStatement(s);
            if (!rs.first())
                return null;
            ans.put(authorA, 0);
            list.add(authorA);
            while (list.size() > 0) {
                String name = list.removeFirst();
                s = "Select ca.nameB from CO_Author ca where ca.nameA=\"" + name
                        + "\"";
                rs = sql.executeStatement(s);
                if (!rs.first())
                    continue;
                while (!rs.isAfterLast())
                {
                    String newname=rs.getString("nameB");
                    if (!ans.containsKey(newname))
                    {
                        if (newname.equals(authorB))
                            return ans.get(name)+1;
                        ans.put(newname, ans.get(name)+1);
                        list.add(newname);
                    }
                    rs.next();
                }
            }
        } catch (SQLException e) {
            return null;
        }
        return -1;
    }

    public Info.BookPackage topMBookPackage(int m) {
        Info.BookPackage ret = new Info.BookPackage();
        String s = "Select * from Book B " + "ORDER BY sold DESC";

        ResultSet rs = sql.executeStatement(s);
        LinkedList<Book> books = new LinkedList<>();
        try {
            if (!rs.first())
                ret.books = new Book[0];
            else {
                int ii = 0;
                while (!rs.isAfterLast() && ii < m) {
                    ii++;
                    Book book = new Book();
                    book.ISBN = rs.getString("ISBN");
                    book.copies = rs.getInt("copies");
                    book.format = rs.getString("format");
                    book.keywords = rs.getString("keywords");
                    book.Price = rs.getDouble("Price");
                    book.publisher = rs.getString("publisher");
                    book.sold = rs.getInt("sold");
                    book.subject = rs.getString("subject");
                    book.title = rs.getString("title");
                    book.year = rs.getInt("year");
                    books.add(book);
                    rs.next();
                }
                ret.books = new Book[books.size()];
                for (int i = 0; i < books.size(); i++)
                    ret.books[i] = books.get(i);
            }
        } catch (SQLException e) {
            return null;
        }
        s = "Select A.name, SUM(B.sold) AS popular from AuthorOf AO, Book B, Author A "
                + "WHERE A.name=AO.name AND B.ISBN=AO.ISBN "
                + "GROUP BY A.name "
                + "ORDER BY popular DESC";
        rs = sql.executeStatement(s);
        LinkedList<String> authors = new LinkedList<>();
        try {
            if (!rs.first())
                ret.authors = new String[0];
            else {
                int ii = 0;
                while (!rs.isAfterLast() && ii < m) {
                    ii++;
                    authors.add(rs.getString("name"));
                    rs.next();
                }
                ret.authors = new String[authors.size()];
                for (int i = 0; i < authors.size(); i++)
                    ret.authors[i] = authors.get(i);
            }
        } catch (SQLException e) {
            return null;
        }
        s = "Select B.publisher, SUM(B.sold) AS popular from Book B "
                + "GROUP BY publisher" + " ORDER BY popular DESC";
        rs = sql.executeStatement(s);
        LinkedList<String> publishers = new LinkedList<>();
        try {
            if (!rs.first())
                ret.publishers = new String[0];
            else {
                int ii = 0;
                while (!rs.isAfterLast() && ii < m) {
                    ii++;
                    publishers.add(rs.getString("publisher"));
                    rs.next();
                }
                ret.publishers = new String[publishers.size()];
                for (int i = 0; i < publishers.size(); i++)
                    ret.publishers[i] = publishers.get(i);
            }
        } catch (SQLException e) {
            return null;
        }
        return ret;
    }

    public Info.PeoplePackage topMPeoplePackage(int m) {
        Info.PeoplePackage ret = new Info.PeoplePackage();
        String s = "Select C.fullName, SUM(O.isTrust) AS trustful from Customer C, Opinion O "
                + "WHERE C.username=O.userA "
                + "GROUP BY C.username "
                + "ORDER BY trustful DESC";
        System.out.println(s);
        ResultSet rs = sql.executeStatement(s);
        LinkedList<String> trustfulusers = new LinkedList<>();
        try {
            if (!rs.first())
                ret.trustful = new String[0];
            else {
                int ii = 0;
                while (!rs.isAfterLast() && ii < m) {
                    ii++;
                    trustfulusers.add(rs.getString("fullName"));
                    rs.next();
                }
                ret.trustful = new String[trustfulusers.size()];
                for (int i = 0; i < trustfulusers.size(); i++)
                    ret.trustful[i] = trustfulusers.get(i);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
        s = "Select C.fullName,AVG(FR.isUseful) AS usefulness from Customer C, FeedBackRe FR "
                + "WHERE C.username=FR.loginOfRe "
                + "GROUP BY C.username "
                + "Order by usefulness DESC";
        rs = sql.executeStatement(s);
        LinkedList<String> usefulusers = new LinkedList<>();
        try {
            if (!rs.first())
                ret.useful = new String[0];
            else {
                int ii = 0;
                while (!rs.isAfterLast() && ii < m) {
                    ii++;
                    usefulusers.add(rs.getString("fullName"));
                    rs.next();
                }
                ret.useful = new String[usefulusers.size()];
                for (int i = 0; i < usefulusers.size(); i++)
                    ret.useful[i] = usefulusers.get(i);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
        return ret;
    }


    public static void main(String[] args) {
        Scanner cin = new Scanner(System.in);
        PrintStream cout = System.out;
        Solver solver = new Solver();
        while (true) {
            String s = cin.next();
            if (s.equals("login")) {
                String username = "cy";
                String pass = "lym";
                cout.println(solver.login(username, pass));
            } else if (s.equals("create")) {
                Info.Customer cus = new Info.Customer();
                cus.username = "lym";
                cus.password = "cy";
                cus.fullName = "cylym";
                cus.address = "Shanghai, China";
                cus.phoneNum = "1234567";
                cout.println(solver.create(cus));
            } else if (s.equals("order")) {
                String ISBN = "123456";
                int num = 6;
                cout.println(solver.order(ISBN, num));
            } else if (s.equals("addBook")) {
                Info.Book book = new Info.Book();
                book.ISBN = "123456";
                book.title = "shi wan ge leng xiao hua~";
                book.publisher = "zhong guo ren min chu ban she";
                book.year = 123;
                book.copies = 5;
                book.sold = 0;
                book.Price = 998.98;
                book.format = "xiao si ni bu chang ming";
                book.keywords = "en";
                book.subject = "dou bi lei";
                LinkedList<Author> au = new LinkedList<>();
                Author a = new Author();
                Author b = new Author();
                a.name = "cy";
                b.name = "lym";
                au.add(a);
                au.add(b);
                cout.println(solver.addBook(book, au));
            } else if (s.equals("changeCopy")) {
                String ISBN = "123456";
                int copies = 7;
                cout.println(
                solver.changeCopy(ISBN, copies));

            } else if (s.equals("feedback")) {
                String ISBN = "123456";
                String text = "very good!";
                int rate = 10;
                cout.println(solver.feedback(ISBN, text, rate));
            } else if (s.equals("feedbackR")) {
                String ISBN = "123456";
                String username = "cy";
                Integer isUseful = 10;
                cout.println(solver.feedbackUseful(ISBN, username, isUseful));
            } else if (s.equals("userTrustfull")) {
                String username = "lym";
                boolean isTrustfull = false;
                cout.println(
                solver.userTrustfull(username, isTrustfull));
            } else if (s.equals("search")) {
                String author = null;
                String publisher = "zhong guo ren min chu ban she";
                String title = "leng";
                String subject = "dou bi lei";
                char ch = 'c';
                Book[] books = solver.search(author, publisher, title, subject, ch);
                for (Book i : books) {
                    cout.println(i);
                }

            } else if (s.equals("topMP")) {
                cout.println(solver.topMPeoplePackage(2));
            
            } else if (s.equals("topMB")) {
                cout.println(solver.topMBookPackage(3));
            } else if (s.equals("recommend")) {
                cout.println(solver.recommendation("123456").length);
            } else if (s.equals("sf")) {
                FeedBack[] fb = solver.search("123456", 2);
                for (FeedBack i : fb)
                cout.println(i);

            } else if (s.equals("bfs")) {
               String aname = "lym";
                String bname = "cy";
                int i = solver.bfs(aname, bname);
                cout.println(i);



            } else
            
            {
                cout.println("done.");
                break;
            }
        }
        cin.close();
    }
}
