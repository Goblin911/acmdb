package main;

public class Lib {
    static String welcome = "我要用中文饿。。。。选个你要用的操作～～";
    static String[] cases = {"sudo",  "login", "logout", "registration", "order",
            "newBook", "addBookCopies",
            "feedback", "feedbackUseful",
            "userTrustful", "searchBook",
            "searchFeedback", "recommendation",
            "distense", "topMBooks", "topMPeople" };
    static int[] power = { 0, 0, 1, 0, 1, 2, 2, 1, 1, 1, 0, 0, 1, 0, 0, 0};
    
    static Integer getNum(String s) {
        for (int i = 0; i < cases.length; i++) {
            if (cases[i].equals(s))
                return power[i]; 
        }
        return null;
    }
    static int topicNum = 15;


    static String[] help = {
        "管理员权限，这样你就可以加书了。密码是root，别怪我没告诉你哦",
        "就是登录啦，一会要输入username password",
        "登出。。。你懂得",
        "注册一个新用户", 
        "买书，要有ISBN，还要告诉我买几本才行。",
        "添加一本书",
        "给书增加库存",
        "添加评论",
        "说这个评论useful。。。",
        "说这个用户trustful。。。",
        "查找一本书，支持模糊查询哦",
        "查找一本书的评论", 
        "说本你喜欢的书，我会猜到你喜欢的书哦",
        "问两个作者的距离，就是他们共同写作的那个距离。就是一张图，如果他们共同写本书，就有一条边，然后最短路么。",
        "前M本好书～",
        "前M个帅哥～"
    };
}
