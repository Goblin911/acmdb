package main;

import java.io.*;
import java.util.*;
import cysql.*;
import cysql.Info.*;

public class Main {
    static Scanner cin = new Scanner(System.in);
    static PrintStream cout = System.out;
    Solver solver;

    int level = 0;
    int logined = 1;
    int isRoot = 2;

    void login() throws Exception {
        int t = Lib.getNum("login");
        if ((level & t) < t)
            throw new Exception();
        cout.println("input your username:");
        String username = cin.nextLine();
        cout.println("input your password:");
        String pass = cin.nextLine();
        int stat = solver.login(username, pass);
        if (stat == 0) {
            cout.println("no such user");
        } else if (stat == 1) {
            cout.println("login successful");
            level |= logined;
        } else {
            cout.println("wrong password");
        }
    }

    void logout() throws Exception {
        int t = Lib.getNum("logout");
        if ((level & t) < t)
            throw new Exception();

        level &= ~logined;
    }

    void sudo() {
        cout.println("Please input root password: ");
        String s = cin.nextLine();
        if (s.equals("root")) {
            level |= isRoot;
        } else {
            cout.println("wrong password");
        }
    }

    void create() {
        Customer cus = new Customer();
        cout.println("Please input username: ");
        cus.username = cin.nextLine();
        cout.println("Please input password: ");
        cus.password = cin.nextLine();
        cout.println("Please input fullname: ");
        cus.fullName = cin.nextLine();
        cout.println("Please input address: ");
        cus.address = cin.nextLine();
        cout.println("Please input phoneNum: ");
        cus.phoneNum = cin.nextLine();
        boolean stat = solver.create(cus);
        if (stat) {
            cout.println("create user successful");
            level |= logined;
        } else
            cout.println("create user failed");
    }

    void newBook() throws Exception {
        int tt = Lib.getNum("newBook");
        if ((level & tt) < tt)
            throw new Exception();

        Book b = new Book();
        cout.println("Please input ISBN: ");
        b.ISBN = cin.nextLine();
        cout.println("Please input title: ");
        b.title = cin.nextLine();
        cout.println("Please input publisher: ");
        b.publisher = cin.nextLine();
        cout.println("Please input year: ");
        b.year = Integer.parseInt(cin.nextLine());
        cout.println("Please input copies: ");
        b.copies = Integer.parseInt(cin.nextLine());
        b.sold = 0;
        cout.println("Please input price: ");
        b.Price = Double.parseDouble(cin.nextLine());
        cout.println("Please input format: ");
        b.format = cin.nextLine();
        cout.println("Please input keywords: ");
        b.keywords = cin.nextLine();
        cout.println("Please input subject: ");
        b.subject = cin.nextLine();
        LinkedList<Author> a = new LinkedList<>();
        for (;;) {
            Author t = new Author();
            cout.println("Please input author name(empty line means over): ");
            t.name = cin.nextLine();
            if (t.name.equals(""))
                break;
            a.add(t);
        }
        boolean stat = solver.addBook(b, a);
        if (stat)
            cout.println("add a new book successful.");
        else
            cout.println("add book failed.");
    }

    void addCopies() throws Exception {
        int tt = Lib.getNum("addCopies");
        if ((level & tt) < tt)
            throw new Exception();
        cout.println("Please input ISBN:");
        String ISBN = cin.nextLine();
        cout.println("Please input copies to add:");
        int i = Integer.parseInt(cin.nextLine());
        boolean stat = solver.changeCopy(ISBN, i);
        if (stat) {
            cout.println("add copies successful.");
        } else {
            cout.println("add copies failed.");
        }
    }

    void feedback() throws Exception {
        int tt = Lib.getNum("feedback");
        if ((level & tt) < tt)
            throw new Exception();
        cout.println("Please input ISBN:");
        String isbn = cin.nextLine();
        cout.println("Please input text:");
        String text = cin.nextLine();
        cout.println("Please input rate:");
        int i = Integer.parseInt(cin.nextLine());
        boolean stat = solver.feedback(isbn, text, i);
        if (stat) {
            cout.println("add feedback successful.");
        } else {
            cout.println("add feedback failed.");
        }
    }

    void feedbackUserful() throws Exception {
        int tt = Lib.getNum("feedbackUserful");
        if ((level & tt) < tt)
            throw new Exception();
        cout.println("Please input ISBN:");
        String isbn = cin.nextLine();
        cout.println("Please input username:");
        String username = cin.nextLine();
        cout.println("Please input isUseful:");
        int i = Integer.parseInt(cin.nextLine());
        boolean stat = solver.feedbackUseful(isbn, username, i);

    }

    void userTrustfull() throws Exception {
        int tt = Lib.getNum("userTrustfull");
        if ((level & tt) < tt)
            throw new Exception();
        cout.println("Please input username:");
        String uname = cin.nextLine();
        cout.println("Please input isTrustfull(0 or 1):");
        String isTrustfull = cin.nextLine();
        boolean stat = solver.userTrustfull(uname, isTrustfull.equals("1"));

    }

    void searchBook() throws Exception {
        int tt = Lib.getNum("searchBook");
        if ((level & tt) < tt)
            throw new Exception();

        cout.println("Please input author name(empty line means ignore):");
        String author = cin.nextLine();
        if (author.equals(""))
            author = null;
        cout.println("Please input publisher name(empty line means ignore):");
        String pub = cin.nextLine();
        if (pub.equals(""))
            pub = null;
        cout.println("Please input title(empty line means ignore):");
        String title = cin.nextLine();
        if (title.equals(""))
            title = null;
        cout.println("Please input subject(empty line means ignore):");
        String sub = cin.nextLine();
        if (sub.equals(""))
            sub = null;
        cout.println("Please input sort fang shi(a or b or c):");
        String ch = cin.nextLine();
        Book[] b = solver.search(author, pub, title, sub, ch.charAt(0));
        for (Book i : b)
            cout.println(i);
    }

    void searchFeedback() throws Exception {
        int tt = Lib.getNum("searchFeedback");
        if ((level & tt) < tt)
            throw new Exception();

        cout.println("Please input ISBN:");
        String isbn = cin.nextLine();
        cout.println("Please input max Number:");
        int num = Integer.parseInt(cin.nextLine());
        FeedBack[] fb = solver.search(isbn, num);
        for (FeedBack i : fb) {
            cout.println(i);
        }
    }

    void recommendation() throws Exception {
        int tt = Lib.getNum("recommendation");
        if ((level & tt) < tt)
            throw new Exception();

        cout.println("Please input ISBN:");
        String isbn = cin.nextLine();
        Book[] b = solver.recommendation(isbn);
        for (Book i : b) {
            cout.println(i);
        }
    }

    void bfs() {
        cout.println("Please input authorA:");
        String a = cin.nextLine();
        cout.println("Please input authorB:");
        String b = cin.nextLine();
        int t = solver.bfs(a, b);
        cout.println("two authors' distense is " + t);
    }

    void topMBooks() {
        cout.println("Please input number of books:");
        String a = cin.nextLine();
        int b = Integer.parseInt(a);
        Info.BookPackage t = solver.topMBookPackage(b);
        cout.println(t);
    }

    void topMPeople() throws Exception {
        int tt = Lib.getNum("recommendation");
        if ((level & tt) < tt)
            throw new Exception();

        cout.println("Please input number of people:");
        String a = cin.nextLine();
        int b = Integer.parseInt(a);
        Info.PeoplePackage t = solver.topMPeoplePackage(b);
        cout.println(t);
    }

    void order() {
        cout.println("Please input isbn:");
        String isbn = cin.nextLine();
        cout.println("Please input number to order:");
        String t = cin.nextLine();
        int m = Integer.parseInt(t);
        int num = solver.order(isbn, m);
        if (num == -1) {
            cout.println("no such book!");
        } else {
            cout.println("order " + num + " books successful");
        }
    }

    void trigger(String command) {
        try {
            switch (command) {
            case "login":
                login();
                break;
            case "registration":
                create();
                break;
            case "newBook":
                newBook();
                break;
            case "addBookCopies":
                addCopies();
                break;
            case "feedback":
                feedback();
                break;
            case "feedbackUsefull":
                feedbackUserful();
                break;
            case "userTrustfull":
                userTrustfull();
                break;
            case "order":
                order();
                break;
            case "searchBook":
                searchBook();
                break;
            case "searchFeedback":
                searchFeedback();
                break;
            case "recommendation":
                recommendation();
                break;
            case "distense":
                bfs();
                break;
            case "topMBooks":
                topMBooks();
                break;
            case "topMPeople":
                topMPeople();
                break;
            case "logout":
                logout();
                break;
            case "exit":
                cout.println("bye.");
                System.exit(0);
            case "sudo":
                sudo();
                break;
            default:
                cout.println("unknown operation!");
            }
        } catch (Exception e) {
            cout.println("illigel operation!");
        }
    }

    void run() {
        cout.println(Lib.welcome);
        while (true) {
            cout.println("you can use the command below.");
            for (int i = 0; i < Lib.topicNum; i++) {
                int t = Lib.getNum(Lib.cases[i]);
                if ((t & level) == t) {
                    cout.println(Lib.cases[i] + "\t" + Lib.help[i]);
                }
            }
            cout.print("mysql> ");
            String command = cin.nextLine();
            trigger(command);
        }
    }

    /**
     *
     */
    public Main() {
        solver = new Solver();
    }

    public static void main(String[] args) {
        Main m = new Main();
        m.run();
    }
}
